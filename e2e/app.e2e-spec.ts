import { Atividade02Page } from './app.po';

describe('atividade02 App', () => {
  let page: Atividade02Page;

  beforeEach(() => {
    page = new Atividade02Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
