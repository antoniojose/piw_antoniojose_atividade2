import { Component } from '@angular/core';


@Component({
  selector:'publicacao',
  templateUrl:'publicacao.component.html',
  styleUrls:['publicacao.component.css']
})
export class PublicacaoComponent {

  publicacoes : Object[];

  constructor() {
    this.publicacoes = [];
    this.iniciar();
  }

  iniciar() : void {
    let a = new Publicacao(),
        b = new Publicacao(),
        c = new Publicacao(),
        d = new Publicacao();

        a.setNome("Usuario A");
        a.setMsg("Mensagem do Usuário A");

      let ca1 = new Comentario(),
          ca2 = new Comentario(),
          ca3 = new Comentario();

          ca1.setMsg("Gostei não do que você disse");
          ca1.setNome("Antonio José");

          ca2.setMsg("Não acredito que tô lendo isso");
          ca2.setNome("Paula Fernandes");

          ca3.setMsg("Isso da processo");
          ca3.setNome("Felipe Carvalho");

        a.addComentario(ca1);
        a.addComentario(ca2);
        a.addComentario(ca3);


        b.setNome("Usuario B");
        b.setMsg("Mensagem do Usuário B");

        c.setNome("Usuario C");
        c.setMsg("Mensagem do Usuário C");

        d.setNome("Usuario D");
        d.setMsg("Mensagem do Usuário D");

        this.publicacoes.push( a );
        this.publicacoes.push( b );
        this.publicacoes.push( c );
        this.publicacoes.push( d );

  }

  add( obj : Object) : boolean {
    let posicao = this.publicacoes.push( obj );
    return (posicao>=0);
  }

  ler( posicao : number ) : Object {

    return this.publicacoes[posicao];
  }

  existe( obj : Object) : boolean {
    let posicao = this.publicacoes.indexOf(obj);
    return (posicao>=0);
  }

  posicaoValida( posicao ) : boolean {
    if (typeof this.publicacoes[posicao] !== 'undefined' && this.publicacoes[posicao] !== null) {
      return true;
    }
    return false;
  }


  getComments() : void {

  }

  salvar() : void {
    let nova = new Publicacao();
      nova.setNome("usuario");
      nova.setMsg("Texto Qualquer");
      this.add(nova);
  }

}

class Comentario {

  private nome : string;
  private msg : string;

  constructor() {
    this.nome = "";
    this.msg = "";
  }

  getNome() : string {
    return this.nome;
  }

  getMsg() : string {
    return this.msg;
  }

  setNome( arg : string) : void {
    this.nome = arg;
  }

  setMsg( arg : string) : void {
    this.msg = arg;
  }

}

class Publicacao {

  private nome : string;
  private msg : string;

  private comentarios : Object[];

  constructor() {
    this.nome  = "";
    this.msg   = "";
    this.comentarios = new Array();
  }


  addComentario( c : Comentario ) : void {
    this.comentarios.push( c );
  }

  getComentarios()  {
    return this.comentarios;
  }

  getSizeComentario() : number {
    return this.comentarios.length;
  }



  getNome() : string {
    return this.nome;
  }

  getMsg() : string {
    return this.msg;
  }

  setNome( arg : string) : void {
    this.nome = arg;
  }

  setMsg( arg : string) : void {
    this.msg = arg;
  }
}
