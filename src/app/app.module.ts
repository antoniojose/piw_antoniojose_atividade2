import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NovaPublicacaoComponent } from './nova-publicacao/nova-publicacao.component';
import { PublicacaoComponent } from './publicacao/publicacao.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    NovaPublicacaoComponent,
    PublicacaoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
