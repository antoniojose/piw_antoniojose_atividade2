
import { Component } from '@angular/core';

@Component({
  selector:"nova-pub",
  templateUrl:"nova-publicacao.component.html",
  styleUrls:["nova-publicacao.component.css"]
})
export class NovaPublicacaoComponent {


  constructor() {

  }

  salvar() : void {
    console.log("Salvando a publicação!");
  }

}
